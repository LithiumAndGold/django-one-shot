from django.shortcuts import render, get_object_or_404, redirect
from todos.forms import TodoForm, CreateForm, CreateItem, TodoItemForm
from todos.models import TodoList, TodoItem


# The view function is the place where we get the data that we want to show in
# the HTML.
def todo_list_list(request):
    todolists = TodoList.objects.all()
    context = {
        "todo_list_list": todolists
    }
    return render(request, "todos/list.html", context)


def show_todo_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_object": detail
    }
    return render(request, "todos/detail.html", context)


def detail_view(request, id):
    detail = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todos/detail.html", id=list.id)
    else:
        form = TodoForm(instance=detail)

    context = {
        "form": form
    }

    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todos/detail.html", id=id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = CreateItem(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("show_todo_detail", id=item.list.id)
    else:
        form = CreateItem()

    context = {
        "form": form,
    }

    return render(request, "todos/items/create.html", context)


def edit_todo_item(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            items = form.save()
            items.save()
            return redirect("show_todo_detail", id=items.list.id)

    else:
        form = TodoItemForm(instance=item)
    context = {
        "form": form
    }

    return render(request, "todos/items/edit.html", context)


def edit_todo_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = CreateForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("show_todo_detail", id=id)
    else:
        form = CreateForm(instance=list)

    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_todo_list(request, id):
    yeet = TodoList.objects.get(id=id)
    if request.method == "POST":
        yeet.delete()
        return redirect("todos/list.html")

    return render(request, "todos/delete.html")
