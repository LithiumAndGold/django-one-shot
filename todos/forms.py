from django.forms import ModelForm
from todos.models import TodoItem, TodoList


class TodoForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
        ]


class CreateForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class CreateItem(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
