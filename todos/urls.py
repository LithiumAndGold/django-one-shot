from django.urls import path
from todos.views import (todo_list_list, show_todo_detail, todo_list_create,
    edit_todo_list, delete_todo_list, todo_item_create, edit_todo_item)


urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", show_todo_detail, name="show_todo_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:id>/edit/", edit_todo_list, name="edit_todo_list"),
    path("<int:id>/delete/", delete_todo_list, name="delete_todo_list"),
    path("items/create/", todo_item_create, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_todo_item, name="todo_item_update"),
]
